<a name="consultancy_be"></a>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/josephhilby/consultancy_be">
    <img src="lib/assets/consultancy.jpeg" alt="Logo" width="400" height="400">
  </a>

<h1 align="center">consultancy_be</h1>
</div>

<!-- ABOUT THE PROJECT -->
## About The Project

[...]

### Built With

* Ruby ~ 2.7.4
* Rails ~ 5.2.8

<!-- Phases -->
## Phases

[Turing Project Requirements](https://backend.turing.edu/module3/projects/consultancy/project_goals)

<!-- CONTACT -->
## Contact

Alex M. -

Bryan K. -

Joseph H. - Joseph.Hilby@gmail.com

Mostafa S. -

<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

* Turing School of Software Design: [https://turing.edu/](https://turing.edu/)
